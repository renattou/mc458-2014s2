#include "quicksort.h"
#include <stdlib.h>

void insertionSort(int v[], int n) {
    int i, j, key;
    for (j = 1; j < n; j++) {
        key = v[j];
        i = j - 1;
        while (i >= 0 && v[i] > key) {
            g_num_comparacoes++;
            v[i+1] = v[i];
            i = i-1;
            g_num_trocas++;
        }
        g_num_comparacoes++;
        v[i+1] = key;
        g_num_trocas++;
    }
}

void exchange(int v[], int a, int b) {
    int aux;
    aux = v[a];
    v[a] = v[b];
    v[b] = aux;
    g_num_trocas++;
}

void random_pivot(int v[], int pivot[], int index[], int n, int p) {
    int i, j, check;
    // puts a random number at index vector. if index is repeated, check for unrepeated index.
    for (int i = 0; i < p; i++) {
        check = 0;
        while (check == 0) {
            index[i] = rand() % n;
            check = 1;
            for (j = 0; j < i; j++) {
                if (index[i] == index[j])
                    check = 0;
            }
        }
    }
    // puts pivot values into pivot array
    for (i = 0; i < p; i++)
        pivot[i] = v[index[i]];
    // sort
    insertionSort(pivot, p);
    insertionSort(index, p);
}

void update_pivot(int v[], int pivot[], int index[], int p) {
    // puts random ordered pivots into array
    int i;
    for (i = 0; i < p; i++)
        v[index[i]] = pivot[i];
}

int separa1(int v[], int n) {
    // c = pivot.
    int c = v[0], i = 1, j = n-1;
    while (i <= j) {
        if (v[i] <= c){
            ++i;
            g_num_comparacoes++;
        }
        else if (c < v[j]) {
            --j;
            g_num_comparacoes += 2;
        }
        else {
            exchange(v, i, j);
            ++i; --j;
            g_num_comparacoes += 2;
        }
    }
    
    // now i == j+1                 
    v[0] = v[j], v[j] = c;
    return j; 
}

void separa2(int v[], int n, int *pivot1, int *pivot2) {
    int i = 0, lowIndex = 0, highIndex = n-1; // iterator and index to start with
    int lt, gt; // position less than p1, position greater than p2
    int p1 = v[lowIndex]; // pivot 1
    int p2 = v[highIndex]; // pivot 2

    // update pivot 1 if p1 >= p2
    if (p1 > p2) {
        exchange(v, lowIndex, highIndex);
        p1 = v[lowIndex];
        p2 = v[highIndex];
    }
    g_num_comparacoes++;

    // compare and trade
    i = lt = lowIndex+1;
    gt = highIndex-1;
    while (i <= gt) {
        if (v[i] < p1) {
            exchange(v, i++, lt++);
            g_num_comparacoes++;
        }
        else if (v[i] > p2) {
            exchange(v, i, gt--);
            g_num_comparacoes += 2;
        }
        else {
            i++;
            g_num_comparacoes += 2;
        }
    }

    exchange(v, lowIndex, --lt);
    exchange(v, highIndex, ++gt);
    *pivot1 = lt;
    *pivot2 = gt;
}

void separa3(int v[], int n, int *pivot1, int *pivot2, int *pivot3) {
    int i = 0, lowIndex = 0, midIndex = 1, highIndex = n-1; // iterator and index to start with
    int lt1, lt2, gt; // position less than p1, less than p2, greater than p3
    // pivots
    int p1 = v[lowIndex];
    int p2 = v[lowIndex + 1];
    int p3 = v[highIndex];
    
    // update pivots if not in correct order
    // trade p1 and p2 if needed
    if (p1 > p2) {
        exchange(v, lowIndex, midIndex);
        p1 = v[lowIndex];
        p2 = v[midIndex];
    }
    // trade p2 and p3 if needed
    if (p2 > p3) {
        exchange(v, midIndex, highIndex);
        p2 = v[midIndex];
        p3 = v[highIndex];
    }
    // trade p1 and p2 if needed
    if (p1 > p2) {
        exchange(v, lowIndex, midIndex);
        p1 = v[lowIndex];
        p2 = v[midIndex];
    }
    g_num_comparacoes += 3;
    
    // compare and trade for p2 and p3
    i = lt2 = midIndex+1;
    gt = highIndex-1;
    while (i <= gt) {
        if (v[i] < p2) {
            exchange(v, i++, lt2++);
            g_num_comparacoes++;
        }
        else if (v[i] > p3) {
            exchange(v, i, gt--);
            g_num_comparacoes += 2;
        }
        else {
            i++;
            g_num_comparacoes += 2;
        }
    }
    
    exchange(v, midIndex, --lt2);
    exchange(v, highIndex, ++gt);
    *pivot2 = lt2;
    *pivot3 = gt;
        
    // compare and trade for p1 and p2
    i = lt1 = lowIndex+1;
    gt = lt2-1;
    while (i <= gt) {
        if (v[i] < p1) {
            exchange(v, i++, lt1++);
            g_num_comparacoes++;
        }
        else if (v[i] > p2) {
            exchange(v, i, gt--);
            g_num_comparacoes += 2;
        }
        else {
            i++;
            g_num_comparacoes += 2;
        }
    }
    
    exchange(v, lowIndex, --lt1);
    *pivot1 = lt1;
    
}

int separaH1(int v[], int n, int pos) {
    // put pivot in first position
    exchange(v, 0, pos);
    return separa1(v, n);
}

void separaH2(int v[], int n, int *pivot1, int *pivot2, int pos1, int pos2) {
    // put pivots in first and last positions
    exchange(v, 0, pos1);
    exchange(v, n-1, pos2);
    separa2(v, n, pivot1, pivot2);
}

void separaH3(int v[], int n, int *pivot1, int *pivot2, int *pivot3, int pos1, int pos2, int pos3) {
    // put pivots in first, second and last positions
    exchange(v, 0, pos1);
    exchange(v, 1, pos2);
    exchange(v, n-1, pos3);
    separa3(v, n, pivot1, pivot2, pivot3);
}


void Quicksort1(int v[], int n) {
    int j;
    g_T_ins_sort = 0;
    if (n > 1) {
        j = separa1(v, n);
        Quicksort1(v, j);
        Quicksort1(v+j+1, n-j-1);
    }
}

void Quicksort2(int v[], int n) {
    int p1, p2;
    g_T_ins_sort = 0;
    if (n > 1) {
        separa2(v, n, &p1, &p2);
        Quicksort2(v, p1);
        Quicksort2(v+p1+1, p2-p1-1);
        Quicksort2(v+p2+1, n-p2-1);
    }
}

void Quicksort3(int v[], int n) {
    int p1, p2, p3;
    g_T_ins_sort = 0;
    if (n > 1) {
        separa3(v, n, &p1, &p2, &p3);
        Quicksort3(v, p1);
        Quicksort3(v+p1+1, p2-p1-1);
        Quicksort3(v+p2+1, p3-p2-1);
        Quicksort3(v+p3+1, n-p3-1);
    }
}

void QuicksortH1(int v[], int n) {
    int j;
    g_T_ins_sort = 36;
    if (n < g_T_ins_sort) {
        insertionSort(v, n);
    }
    else if (n > 1) {
        // gets random pivots
        int pivot[3], index[3];
        random_pivot(v, pivot, index, 3, 3);
        update_pivot(v, pivot, index, 3);
        
        j = separaH1(v, n, index[1]);
        QuicksortH1(v, j);
        QuicksortH1(v+j+1, n-j-1);
    }
}

void QuicksortH2(int v[], int n) {
    int p1, p2;
    g_T_ins_sort = 38;
    if (n < g_T_ins_sort) {
        insertionSort(v, n);
    }
    else if (n > 1) {
        // gets random pivots
        int pivot[5], index[5];
        random_pivot(v, pivot, index, 5, 5);
        update_pivot(v, pivot, index, 5);
        
        separaH2(v, n, &p1, &p2, index[1], index[3]);
        QuicksortH2(v, p1);
        QuicksortH2(v+p1+1, p2-p1-1);
        QuicksortH2(v+p2+1, n-p2-1);
    }
}

void QuicksortH3(int v[], int n) {
    int p1, p2, p3;
    g_T_ins_sort = 39;
    if (n < g_T_ins_sort) {
        insertionSort(v, n);
    }
    else if (n > 1) {
        // gets random pivots
        int pivot[7], index[7];
        random_pivot(v, pivot, index, 7, 7);
        update_pivot(v, pivot, index, 7);
        
        separaH3(v, n, &p1, &p2, &p3, index[1], index[3], index[5]);
        QuicksortH3(v, p1);
        QuicksortH3(v+p1+1, p2-p1-1);
        QuicksortH3(v+p2+1, p3-p2-1);
        QuicksortH3(v+p3+1, n-p3-1);
    }
}

