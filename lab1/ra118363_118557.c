#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define N_MAX 16384

int add_count = 0;
int multi_count = 0;

int nextPow2(int x);
double complex* fft(int n, double complex root, double complex M[]);
double complex rootN(int n);
double complex* doubleToComplexArr(double A[], int n, int maxsize);
void zeroArray(double A[], int n);
double complex* mult(double complex *M, double complex *N, int n);
double complex* fftInv(int n, double complex root, double complex M[]);
void recursive_kara(int n, double *A, double *B, double *C);

void multpoli_quad(int n, double A[], double B[], double C[]) {
    int k, i;
    
    C[0] = A[0]*B[0];
    multi_count += 1;
    
    for (k=1; k<n; k++) {
        // first half
        C[k] = A[0] * B[k]; // i = 0
        for (i=1; i<=k; i++) {
            C[k] += A[i] * B[k-i];
        }
        // second half
        C[n-1+k] = A[k] * B[n-1]; // i = 0
        for (i=1; i<n-k; i++) {
            C[n-1+k] += A[i+k] * B[n-1-i];
        }
    }
    add_count += (n-1) * (n-1);
    multi_count += (n-1) * (n+1);
}

void multpoli_kara(int n, double A[], double B[], double C[]) {
    int nMax = nextPow2(n);
    // calls recursive Karatsuba method.
    recursive_kara(nMax, A, B, C);
}

void recursive_kara(int n, double *A, double *B, double *C) {
    // (ax + b) * (cx + d) case
    if (n == 2) {
        C[0] = A[0] * B[0];
        C[2] = A[1] * B[1];
        C[1] = (A[0] + A[1]) * (B[0] + B[1]) - C[0] - C[2];
        C[3] = 0;
        add_count += 4;
        multi_count += 3;
        return;
    }
    
    double *AA, *BB, *Z0, *Z1, *Z2;
    int i, m = n/2;
    
    // allocating temporary arrays
    AA = (double*) malloc(m * sizeof(double));
    BB = (double*) malloc(m * sizeof(double));
    Z0 = (double*) malloc(n * sizeof(double));
    Z1 = (double*) malloc(n * sizeof(double));
    Z2 = (double*) malloc(n * sizeof(double));
    
    for (i = 0; i < m; i++) {
        AA[i] = A[i] + A[i+m]; // AA = Alow + Ahigh
        BB[i] = B[i] + B[i+m]; // BB = Blow + Bhigh
    }
    add_count += 2 * m;
    
    recursive_kara(m, A, B, Z0); // recursive call for first half
    recursive_kara(m, A + m, B + m, Z2); // recursive call for second half
    recursive_kara(m, AA, BB, Z1); // recursive call for sum of halfs
    
    for (i = 0; i < n; i++) {
        Z1[i] -= Z0[i] + Z2[i];
    }
    add_count += 2 * n;
    
    // summing recursive results in C
    for (i = 0; i < m; i++) {
        C[i] = Z0[i];
        C[i+m] = Z0[i+m] + Z1[i];
        C[i+n] = Z2[i] + Z1[i+m];
        C[i+n+m] = Z2[i+m];
    }
    add_count += 2 * m;
    
    free(AA);
    free(BB);
    free(Z0);
    free(Z1);
    free(Z2);
}

void multpoli_fft(int n, double A[], double B[], double C[]) {
    int i;
    int maxsize = nextPow2(2*n-1)   ;
    double complex *M, *Mt, *N, *Nt, *R, *T;
    double complex root = rootN (maxsize);
    
    // calculates fft(M), fft(N), multiply them and applies the inverse fft
    M = doubleToComplexArr(A, n, maxsize);
    Mt = fft(maxsize, root, M);
    N = doubleToComplexArr(B, n, maxsize);
    Nt = fft(maxsize, root, N);
    R = mult(Mt, Nt, maxsize);
    
    T = fftInv (maxsize, root, R);
    
    // passing real values to double array
    for (i=0; i<maxsize; i++)
        C[i] = creal(T[i]);
    
    free(M);
    free(Mt);
    free(N);
    free(Nt);
    free(R);
    free(T);
}

double complex* fft(int n, double complex root, double complex M[]) {
    int i, j;
    double complex *Mpar, *Mimpar, *Ypar, *Yimpar, *Y;
    double complex w, wN, wImpar;
    
    // 1 element case
    if (n == 1) {
        Y = (double complex*) malloc(sizeof(double complex));
        Y[0] = M[0];
        return Y;
    }
    
    wN = root;
    w = 1;
    
    // allocating and filling arrays with half of original array
    Mpar = (double complex *) malloc(n/2 * sizeof(double complex));
    Mimpar = (double complex *) malloc(n/2 * sizeof(double complex));
    Y = (double complex *) malloc(n * sizeof(double complex));
    
    for (i=0, j=0; i<n; i+=2, j++)
        Mpar[j] = M[i];
    for (i=1, j=0; i<n; i+=2, j++)
        Mimpar[j] = M[i];
    
    // using recursion on arrays
    Yimpar = fft (n/2, cpow(wN, 2), Mimpar);
    Ypar = fft (n/2, cpow(wN, 2), Mpar);
    
    // performing operations on original array
    for (i=0; i<n/2; i++) {
        wImpar = w * Yimpar[i];
        Y[i] = Ypar[i] + wImpar;
        Y[n/2 + i] = Ypar[i] - wImpar;
        w = w*wN;
    }
    add_count += n;
    multi_count += n;
    
    free(Mpar);
    free(Mimpar);
    free(Ypar);
    free(Yimpar);

    return Y;
}

double complex* fftInv(int n, double complex root, double complex M[]) {
    int i;
    double complex *Y;
    
    // inverse fft = fft for 1/root divided by n
    Y = fft(n, 1/root, M);
    
    for (i=0; i<n; i++)
        Y[i] = Y[i]/n;
    
    return Y;
}

double complex* mult(double complex *M, double complex *N, int n) {
    int i;
    double complex *R = (double complex*) malloc(n * sizeof(double complex));
    
    // multiply complex arrays
    for (i=0; i<n; i++)
        R[i] = M[i] * N[i];
    multi_count += n;
    
    return R;
}

double complex rootN(int n) {
    // returns the n-th root of 1
    double complex x = cos(2*M_PI/n) + I * sin(2*M_PI/n);
    return x;
}

int nextPow2(int x) {
    // returns the least power of 2 bigger than x
    return pow(2, ceil(log(x)/(double)log(2)));
}

double complex* doubleToComplexArr(double A[], int n, int maxsize) {
    int i;
    double complex *Ac = (double complex *) malloc (maxsize * sizeof(double complex));
    
    // converts double array to complex array
    for (i=0; i<n; i++)
        Ac[i] = A[i];
    for (; i<maxsize; i++)
        Ac[i] = 0;
    
    return Ac;
}

void zeroArray(double A[], int n) {
    int i;
    for (i=0; i<n; i++)
        A[i] = 0;
}

int main(int argc, char *argv[]) {
    int i, j;
    int p, nA, nB, nMax;
    double A[N_MAX], B[N_MAX], C[2*N_MAX];
    unsigned long int quad_add_count = 0, kara_add_count = 0, fft_add_count = 0;
    unsigned long int quad_multi_count = 0, kara_multi_count = 0, fft_multi_count = 0;
    unsigned long int quad_time = 0, kara_time = 0, fft_time = 0;
    FILE *dataFile, *fileOut;
    char filename[50];
    clock_t time = clock();
    
    zeroArray(A, N_MAX);
    zeroArray(B, N_MAX);
    zeroArray(C, 2*N_MAX);
    
    // accept just one argument
    if (argc < 1 || argc > 2) {
        printf("Expecting one argument.");
        return 1;
    }
    
    // try to open argument file
    dataFile = fopen(argv[1], "r");
    if (dataFile == NULL) {
        return 1;
    }
    
    // read number of pairs of polynomials
    fscanf(dataFile, " %d", &p);
    
    for (i=0; i<p; i++) {
        // read first polynomial
        fscanf(dataFile, " %d", &nA);
        for (j=0; j<nA; j++)
            fscanf(dataFile, " %lf", &A[j]);
        
        // read second polynomial
        fscanf(dataFile, " %d", &nB);
        for (j=0; j<nB; j++)
            fscanf(dataFile, " %lf", &B[j]);
        
        // get max polynomial size
        nMax = nA > nB ? nA : nB;
        
        // multiply polynomials using direct method
        add_count = multi_count = 0;
        time = clock();
        multpoli_quad(nMax, A, B, C);
        quad_time += clock() - time;
        quad_add_count += add_count;
        quad_multi_count += multi_count;
        zeroArray(C, nMax - 1);
        
        // multiply polynomials using Karatsuba method
        add_count = multi_count = 0;
        time = clock();
        multpoli_kara(nMax, A, B, C);
        kara_time += clock() - time;
        kara_add_count += add_count;
        kara_multi_count += multi_count;
        zeroArray(C, nMax - 1);
        
        // multiply polynomials using FFT method
        add_count = multi_count = 0;
        time = clock();
        multpoli_fft(nMax, A, B, C);
        fft_time += clock() - time;
        fft_add_count += add_count;
        fft_multi_count += multi_count;
        zeroArray(C, nMax - 1);
    }
    
    // generates output file
    strcpy(filename, "ra118363_118557.log");
    if (access(filename, F_OK) != -1)
        fileOut = fopen(filename, "a");
    else
        fileOut = fopen(filename, "w");
    
    fprintf(fileOut, "quad %s %lu %lu %.6f\n", argv[1], quad_multi_count / p,
        quad_add_count / p, (double) quad_time / CLOCKS_PER_SEC / p);
    fprintf(fileOut, "kara %s %lu %lu %.6f\n", argv[1], kara_multi_count / p,
        kara_add_count / p, (double) kara_time / CLOCKS_PER_SEC / p);
    fprintf(fileOut, "fft  %s %lu %lu %.6f\n", argv[1], fft_multi_count / p,
        fft_add_count / p, (double) fft_time / CLOCKS_PER_SEC / p);
    
    fclose(dataFile);
    fclose(fileOut);
    
    return 0;
}
