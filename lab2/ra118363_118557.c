#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

/* types definitions */
typedef unsigned int uint;
typedef unsigned long ulong;
typedef uint **MatrizAdjacencia;
typedef MatrizAdjacencia Madj;

/* global variables to count number of operation */
int add_count = 0;
int multi_count = 0;

uint N;
uint n_call[3];

void alloc_array(uint **p, int n);
void alloc_matrix(uint ***p, int m, int n);
void free_matrix(uint **p, int m);
void copy_array(uint *vc, uint *vo, int n);
void zero_array(uint *v, int n);

void freeA1_A2(uint *v0, uint *v1, Madj M0, Madj M1);
void freeA3(uint *v1, uint *v2, uint *v3, uint *v4, Madj M1, Madj M2, Madj M3, Madj M4);

void copy_matrix(MatrizAdjacencia Mc, MatrizAdjacencia Mo, int n);
void zero_matrix(MatrizAdjacencia M, int n);

int degree(uint *adj, int n);
void add_edge(MatrizAdjacencia M, int n, int m);
void remove_edge(MatrizAdjacencia M, int n, int m);
void remove_vertex(MatrizAdjacencia M, int pos);
void add_vertex_to_solution(MatrizAdjacencia M, uint v[], int pos, int adj_count);
void add_adjacent_to_solution(MatrizAdjacencia M, uint v[], int pos);
int get_vertex_by_degree(MatrizAdjacencia M, uint v[], int min_degree, int max_degree, int *out_count);
int get_adjacent_adjacent_count(MatrizAdjacencia M, int pos);
int get_vertex_with_degree_2(MatrizAdjacencia M, uint v[], int *out_count);
int get_vertex_with_degree_3_neighbours(MatrizAdjacencia M, uint v[], int *adj_pos1, int *adj_pos2, int *adj_pos3);
int get_vertex_with_degree_3_one_adjacent(MatrizAdjacencia M, uint v[], int *adj_pos);
int get_vertex_with_degree_3_two_shared(MatrizAdjacencia M, uint v[], int *other_pos, int *adj_count);
int get_clique_vertex(MatrizAdjacencia M, uint v[], int *out_count);
int get_shared_adjacent_count(MatrizAdjacencia M, int pos1, int pos2);

uint A1(uint n, MatrizAdjacencia M, uint *tam_ci, uint *seq_vert_ci, uint tempo_maximo, int nivel) {
    
    /* 1. if v has no adjacent vertices, v will be in a optimal solution together with the max independent
    set in G-v
    2. if v has at least 1 adjacent vertex, G1 (G1 = G - v - Adj(v)) has at max n-2 vertices. */
    
    uint i, pos;
    uint *v0, *v1;
    uint adj = 0;
    uint time_ok = 1;
    clock_t t_ini = clock(), t_fin;
    double t;
    
    MatrizAdjacencia M1, M2;
    uint tam1, tam2;
    tam1 = tam2 = *tam_ci;
    
    if (n == 0)
        return 1;
    
    for (i = 0; i < N; i++)
        if (seq_vert_ci[i] == 0)
            break;
    pos = i;
    
    /* check if degree is zero. if yes, it's on solution */
    if (degree(M[pos], N) == 0) {
        seq_vert_ci[pos] = 1;
        *tam_ci += 1;
        /* testing time */
        t_fin = clock();
        t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
        if (t < tempo_maximo) {
            return A1(n-1, M, tam_ci, seq_vert_ci, tempo_maximo, nivel+1);
        }
        else
            return 0;
    }
    
    alloc_array(&v0, N);
    alloc_array(&v1, N);
    alloc_matrix(&M1, N, N);
    alloc_matrix(&M2, N, N);
    
    copy_array(v0, seq_vert_ci, N);
    copy_array(v1, seq_vert_ci, N);
    
    /* G0 (solution without v) */
    copy_matrix(M1, M, N);
    /* removing v */
    v0[pos] = 2;
    remove_vertex(M1, pos);
    /* testing time */
    t_fin = clock();
    t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
    if (t < tempo_maximo) {
        n_call[0]++;
        time_ok = A1(n-1, M1, &tam1, v0, (uint)((double)tempo_maximo - t), nivel+1);
    }
    else
        time_ok = 0;
    
    /* G1 (solution with v and without its adjacent vertices) */
    copy_matrix(M2, M, N);
    /* removing vertices adjacents to v */
    for (i = 0; i < N; i++) {
        if (M2[pos][i] == 1) {
            v1[i] = 2;
            remove_vertex(M2, i);
            adj += 1;
        }
    }
    v1[pos] = 1;
    remove_vertex(M2, pos);
    tam2 += 1;
    
    /* testing time */
    if (time_ok) {
        t_fin = clock();
        t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
        if (t < tempo_maximo) {
            n_call[0]++;
            time_ok = A1(n-1-adj, M2, &tam2, v1, (uint)((double)tempo_maximo - t), nivel+1);
        }
        else
            time_ok = 0;
    }
    
    if (tam1 > tam2) {
        *tam_ci = tam1;
        copy_array(seq_vert_ci, v0, N);
    }
    else {
        *tam_ci = tam2;
        copy_array(seq_vert_ci, v1, N);
    }
    
    freeA1_A2(v0, v1, M1, M2);
    return time_ok;
}

uint A2(uint n, MatrizAdjacencia M, uint *tam_ci, uint *seq_vert_ci, uint tempo_maximo) {
    int pos, adj_count;
    uint *v0, *v1;
    uint time_ok = 1;
    clock_t t_ini = clock(), t_fin;
    double t;
    
    MatrizAdjacencia M1, M2;
    uint tam1, tam2;
    tam1 = tam2 = *tam_ci;
    
    if (n == 0)
        return 1;
    
    /* searches for vertex with degree 0 or 1 */
    pos = get_vertex_by_degree(M, seq_vert_ci, 0, 1, &adj_count);
    
    /* if a vertex with degree 0 or 1 is found, add it to the solution */
    if (pos >= 0) {
        add_vertex_to_solution(M, seq_vert_ci, pos, adj_count);
        *tam_ci += 1;
        /* testing time */
        t_fin = clock();
        t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
        if (t < tempo_maximo) {
            n_call[1]++;
            return A2(n-1-adj_count, M, tam_ci, seq_vert_ci, (uint)((double)tempo_maximo - t));
        }
        else
            return 0;
    }
    
    /* searches for vertex with degree 3 or more */
    pos = get_vertex_by_degree(M, seq_vert_ci, 3, N, &adj_count);
    
    /* if a vertex is not found, searches for vertex with degree 2 */
    if (pos < 0) {
        pos = get_vertex_by_degree(M, seq_vert_ci, 2, 2, &adj_count);
        
        add_vertex_to_solution(M, seq_vert_ci, pos, adj_count);
        *tam_ci += 1;
        /* testing time */
        t_fin = clock();
        t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
        if (t < tempo_maximo) {
            n_call[1]++;
            return A2(n-1-adj_count, M, tam_ci, seq_vert_ci, (uint)((double)tempo_maximo - t));
        }
        else
            return 0;
    }
    
    alloc_array(&v0, N);
    alloc_array(&v1, N);
    alloc_matrix(&M1, N, N);
    alloc_matrix(&M2, N, N);
    copy_array(v0, seq_vert_ci, N);
    copy_array(v1, seq_vert_ci, N);
    
    /* G0 (solution without v) */
    copy_matrix(M1, M, N);
    /* removing v */
    v0[pos] = 2;
    remove_vertex(M1, pos);
    /* testing time */
    t_fin = clock();
    t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
    if (t < tempo_maximo) {
        n_call[1]++;
        time_ok = A2(n-1, M1, &tam1, v0, (uint)((double)tempo_maximo - t));
    }
    else
        time_ok = 0;
    
    /* G1 (solution with v and without its adjacent vertices) */
    copy_matrix(M2, M, N);
    /* removing vertices adjacents to v */
    add_vertex_to_solution(M2, v1, pos, adj_count);
    tam2 += 1;
    /* testing time */
    if (time_ok) {
        t_fin = clock();
        t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
        if (t < tempo_maximo) {
            n_call[1]++;
            time_ok = A2(n-1-adj_count, M2, &tam2, v1, (uint)((double)tempo_maximo - t));
        }
        else
            time_ok = 0;
    }
    
    if (tam1 > tam2 && tam1 > *tam_ci) {
        *tam_ci = tam1;
        copy_array(seq_vert_ci, v0, N);
    }
    else if (tam2 > *tam_ci) {
        *tam_ci = tam2;
        copy_array(seq_vert_ci, v1, N);
    }
    
    freeA1_A2(v0, v1, M1, M2);    
    return time_ok;
}

uint A3(uint n, MatrizAdjacencia M, uint *tam_ci, uint *seq_vert_ci, uint tempo_maximo, int pai) {
    int pos, pos2, pos3, pos4, adj_count;
    uint *v1, *v2, *v3, *v4;
    uint time_ok = 1;
    clock_t t_ini = clock(), t_fin;
    double t;
    
    MatrizAdjacencia M1, M2, M3, M4;
    uint tam1, tam2, tam3, tam4;
    tam1 = tam2 = tam3 = tam4 = *tam_ci;
    
    if (n == 0)
        return 1;
    
    /* CASE 1: searches for vertex which neighbours form a clique */
    pos = get_clique_vertex(M, seq_vert_ci, &adj_count);
    
    /* if a clique is found, add the "center" to the solution */
    if (pos >= 0) {
        add_vertex_to_solution(M, seq_vert_ci, pos, adj_count);
        *tam_ci += 1;
        
        /* testing time */
        t_fin = clock();
        t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
        if (t < tempo_maximo) {
            n_call[2]++;
            return A3(n-1-adj_count, M, tam_ci, seq_vert_ci, tempo_maximo, 1);
        }
        else
            return 0;
    }
    
    /* CASE 2: searches for vertex with degree 2
    which neighbours have two neighbours in total */
    pos = get_vertex_with_degree_2(M, seq_vert_ci, &adj_count);
    
    /* if the vertex is found, add the neighbours to the solution */
    if (pos >= 0) {
        add_adjacent_to_solution(M, seq_vert_ci, pos);
        *tam_ci += 2;
        /* testing time */
        t_fin = clock();
        t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
        if (t < tempo_maximo) {
            n_call[2]++;
            return A3(n-2-2, M, tam_ci, seq_vert_ci, tempo_maximo, 2);
        }
        else
            return 0;
    }
    
    /* CASE 3: searches for vertex with degree 2 */
    alloc_array(&v1, N);
    alloc_array(&v2, N);
    alloc_array(&v3, N);
    alloc_array(&v4, N);
    alloc_matrix(&M1, N, N);
    alloc_matrix(&M2, N, N);
    alloc_matrix(&M3, N, N);
    alloc_matrix(&M4, N, N);
    copy_array(v1, seq_vert_ci, N);
    copy_array(v2, seq_vert_ci, N);    
    
    pos = get_vertex_by_degree(M, seq_vert_ci, 2, 2, &adj_count);
    
    if (pos >= 0) {
        /* G1 (solution with v and without its adjacent vertices) */
        copy_matrix(M1, M, N);
        add_vertex_to_solution(M1, v1, pos, adj_count);
        tam1 += 1;
        /* testing time */
        t_fin = clock();
        t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
        if (t < tempo_maximo) {
            n_call[2]++;
            time_ok = A3(n-1-2, M1, &tam1, v1, tempo_maximo, 3);
        }
        else
            time_ok = 0;
        
        /* G2 (solution without v and with its adjacent vertices) */
        copy_matrix(M2, M, N);
        adj_count = get_adjacent_adjacent_count(M2, pos);
        add_adjacent_to_solution(M2, v2, pos);
        tam2 += 2;
        
        /* testing time */
        if (time_ok) {
            t_fin = clock();
            t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
            if (t < tempo_maximo) {
                n_call[2]++;
                time_ok = A3(n-2-adj_count, M2, &tam2, v2, tempo_maximo, 3);
            }
            else
                time_ok = 0;
        }
        
        if (tam1 > tam2) {
            *tam_ci = tam1;
            copy_array(seq_vert_ci, v1, N);
        }
        else {
            *tam_ci = tam2;
            copy_array(seq_vert_ci, v2, N);
        }
        freeA3(v1, v2, v3, v4, M1, M2, M3, M4);
        return time_ok;
    }
    
    /* CASE 4: searches for vertex with degree 4 or more */
    pos = get_vertex_by_degree(M, seq_vert_ci, 4, N, &adj_count);
    
    if (pos >= 0) {
        /* G1 (solution without v) */
        copy_matrix(M1, M, N);
        /* removing v */
        v1[pos] = 2;
        remove_vertex(M1, pos);
        /* testing time */
        if (time_ok) {
            t_fin = clock();
            t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
            if (t < tempo_maximo) {
                n_call[2]++;
                time_ok = A3(n-1, M1, &tam1, v1, tempo_maximo, 4);
            }
            else
                time_ok = 0;
        }
        
        /* G2 (solution with v and without its adjacent vertices) */
        copy_matrix(M2, M, N);
        /* removing vertices adjacents to v */
        add_vertex_to_solution(M2, v2, pos, adj_count);
        tam2 += 1;
        /* testing time */
        if (time_ok) {
            t_fin = clock();
            t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
            if (t < tempo_maximo) {
                n_call[2]++;
                time_ok = A3(n-1-adj_count, M2, &tam2, v2, tempo_maximo, 4);
            }
            else
                time_ok = 0;
        }
        
        if (tam1 > tam2) {
            *tam_ci = tam1;
            copy_array(seq_vert_ci, v1, N);
        }
        else {
            *tam_ci = tam2;
            copy_array(seq_vert_ci, v2, N);
        }
        freeA3(v1, v2, v3, v4, M1, M2, M3, M4);
        return time_ok;
    }
    
    /* in the remaining cases, each node has exactly 3 neighbours */
    
    /* CASE 5: two neighbours are connected to each other and one isn't */
    pos = get_vertex_with_degree_3_one_adjacent(M, seq_vert_ci, &pos2);
    
    if (pos >= 0) {
        /* G1 (solution with v) */
        copy_matrix(M1, M, N);
        add_vertex_to_solution(M1, v1, pos, 3);
        tam1 += 1;
        /* testing time */
        if (time_ok) {
            t_fin = clock();
            t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
            if (t < tempo_maximo) {
                n_call[2]++;
                time_ok = A3(n-1-3, M1, &tam1, v1, tempo_maximo, 5);
            }
            else
                time_ok = 0;
        }
        
        /* G2 (solution with u) */
        copy_matrix(M2, M, N);
        add_vertex_to_solution(M2, v2, pos2, 3);
        tam2 += 1;
        /* testing time */
        if (time_ok) {
            t_fin = clock();
            t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
            if (t < tempo_maximo) {
                n_call[2]++;
                time_ok = A3(n-1-3, M2, &tam2, v2, tempo_maximo, 5);
            }
            else
                time_ok = 0;
        }
        
        if (tam1 > tam2) {
            *tam_ci = tam1;
            copy_array(seq_vert_ci, v1, N);
        }
        else {
            *tam_ci = tam2;
            copy_array(seq_vert_ci, v2, N);
        }
        freeA3(v1, v2, v3, v4, M1, M2, M3, M4);
        return time_ok;
    }
    
    /* CASE 6: two vertices have at least two common neighbours */
    pos = get_vertex_with_degree_3_two_shared(M, seq_vert_ci, &pos2, &adj_count);
    
    if (pos >= 0) {
        /* G1 (solution without v and u) */
        copy_matrix(M1, M, N);
        v1[pos] = v1[pos2] = 2;
        remove_vertex(M1, pos);
        remove_vertex(M1, pos2);
        /* testing time */
        if (time_ok) {
            t_fin = clock();
            t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
            if (t < tempo_maximo) {
                n_call[2]++;
                time_ok = A3(n-2, M1, &tam1, v1, tempo_maximo, 6);
            }
            else
                time_ok = 0;
        }
        
        /* G2 (solution with u and v) */
        copy_matrix(M2, M, N);
        add_vertex_to_solution(M2, v2, pos, 3);
        add_vertex_to_solution(M2, v2, pos2, 3);
        tam2 += 2;
        /* testing time */
        if (time_ok) {
            t_fin = clock();
            t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
            if (t < tempo_maximo) {
                n_call[2]++;
                time_ok = A3(n-2-adj_count, M2, &tam2, v2, tempo_maximo, 6);
            }
            else
                time_ok = 0;
        }
        
        if (tam1 > tam2) {
            *tam_ci = tam1;
            copy_array(seq_vert_ci, v1, N);
        }
        else {
            *tam_ci = tam2;
            copy_array(seq_vert_ci, v2, N);
        }
        freeA3(v1, v2, v3, v4, M1, M2, M3, M4);
        return time_ok;
    }
    
    /* CASE 7: neighbours only share one common neighbour */
    pos = get_vertex_with_degree_3_neighbours(M, seq_vert_ci, &pos2, &pos3, &pos4);
    
    copy_array(v2, seq_vert_ci, N);
    copy_array(v3, seq_vert_ci, N);
    
    /* G1 (solution with u) */
    copy_matrix(M1, M, N);
    add_vertex_to_solution(M1, v1, pos, 3);
    tam1 += 1;
    /* testing time */
    if (time_ok) {
        t_fin = clock();
        t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
        if (t < tempo_maximo) {
            n_call[2]++;
            time_ok = A3(n-1-3, M1, &tam1, v1, tempo_maximo, 7);
        }
        else
            time_ok = 0;
    }
    
    /* G2 (solution with v and w) */
    copy_matrix(M2, M, N);
    add_vertex_to_solution(M2, v2, pos2, 3);
    add_vertex_to_solution(M2, v2, pos3, 3);
    tam2 += 2;
    /* testing time */
    if (time_ok) {
        t_fin = clock();
        t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
        if (t < tempo_maximo) {
            n_call[2]++;
            time_ok = A3(n-2-5, M2, &tam2, v2, tempo_maximo, 7);
        }
        else
            time_ok = 0;
    }
    
    /* G3 (solution with v and x) */
    copy_matrix(M3, M, N);
    add_vertex_to_solution(M3, v3, pos2, 3);
    add_vertex_to_solution(M3, v3, pos4, 3);
    tam3 += 2;
    /* testing time */
    if (time_ok) {
        t_fin = clock();
        t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
        if (t < tempo_maximo) {
            n_call[2]++;
            time_ok = A3(n-2-5, M3, &tam3, v3, tempo_maximo, 7);
        }
        else
            time_ok = 0;
    }
    
    /* G4 (solution with w and x) */
    copy_matrix(M4, M, N);
    add_vertex_to_solution(M4, v4, pos3, 3);
    add_vertex_to_solution(M4, v4, pos4, 3);
    tam4 += 2;
    /* testing time */
    if (time_ok) {
        t_fin = clock();
        t = (double)((t_fin - t_ini)/CLOCKS_PER_SEC);
        if (t < tempo_maximo) {
            n_call[2]++;
            time_ok = A3(n-2-5, M4, &tam4, v4, tempo_maximo, 7);
        }
        else
            time_ok = 0;
    }
    
    if (tam1 > tam2 && tam1 > tam3 && tam1 > tam4) {
        *tam_ci = tam1;
        copy_array(seq_vert_ci, v1, N);
    }
    else if (tam2 > tam3 && tam2 > tam4) {
        *tam_ci = tam2;
        copy_array(seq_vert_ci, v2, N);
    }
    else if (tam3 > tam4) {
        *tam_ci = tam3;
        copy_array(seq_vert_ci, v3, N);
    }
    else {
        *tam_ci = tam4;
        copy_array(seq_vert_ci, v4, N);
    }
    
    freeA3(v1, v2, v3, v4, M1, M2, M3, M4);
    return time_ok;
}

/* returns degree on adjacent matrix line */
int degree(uint *adj, int n) {
    int i, cont = 0;
    for(i = 0; i < n; i++) {
        if (adj[i] == 1)
            cont++;
    }
    return cont;
}

int get_vertex_by_degree(MatrizAdjacencia M, uint v[], int min_degree, int max_degree, int *out_count) {
    int i, j, adj_count;
    
    for (i = 0; i < N; i++) {
        if (v[i] == 0) {
            adj_count = 0;
            for (j = 0; j < N; j++) {
                if (M[i][j] == 1) {
                    adj_count++;
                }
            }
            if (adj_count >= min_degree && adj_count <= max_degree) {
                *out_count = adj_count;
                return i;
            }
        }
    }
    return -1;
}

/* get vertex with degree 2 and adjacent vertex with 2 shared vertex */
int get_vertex_with_degree_2(MatrizAdjacencia M, uint v[], int *out_count) {
    int i, j, adj_count;
    
    for (i = 0; i < N; i++) {
        if (v[i] == 0) {
            adj_count = 0;
            for (j = 0; j < N; j++) {
                if (M[i][j] == 1) {
                    adj_count++;
                }
            }
            if (adj_count == 2 && get_adjacent_adjacent_count(M, i) == 2) {
                *out_count = adj_count;
                return i;
            }
        }
    }
    return -1;
}

/* get vertex with degree 3 and adjacent vertex not connected with the others */
int get_vertex_with_degree_3_one_adjacent(MatrizAdjacencia M, uint v[], int *adj_pos) {
    int i, j, pos1 = -1, pos2 = -1, pos3 = -1;
    
    for (i = 0; i < N; i++) {
        if (v[i] == 0) {
            for (j = 0; j < N; j++) {
                if (M[i][j] == 1) {
                    if (pos1 == -1) {
                        pos1 = j;
                    }
                    else if (pos2 == -1) {
                        pos2 = j;
                    }
                    else {
                        pos3 = j;
                        
                        if (M[pos1][pos2] != 1 && M[pos2][pos3] == 1 && M[pos3][pos1] != 1) {
                            *adj_pos = pos1;
                            return i;
                        }
                        else if (M[pos1][pos2] != 1 && M[pos2][pos3] != 1 && M[pos3][pos1] == 1) {
                            *adj_pos = pos2;
                            return i;
                        }
                        else if (M[pos1][pos2] == 1 && M[pos2][pos3] != 1 && M[pos3][pos1] != 1) {
                            *adj_pos = pos3;
                            return i;
                        }
                    }
                }
            }
        }
    }
    return -1;
}

/* get vertex with degree 3' neighbours */
int get_vertex_with_degree_3_neighbours(MatrizAdjacencia M, uint v[], int *adj_pos1, int *adj_pos2, int *adj_pos3) {
    int i, j, pos1 = -1, pos2 = -1;
    
    for (i = 0; i < N; i++) {
        if (v[i] == 0) {
            for (j = 0; j < N; j++) {
                if (M[i][j] == 1) {
                    if (pos1 == -1) {
                        pos1 = j;
                    }
                    else if (pos2 == -1) {
                        pos2 = j;
                    }
                    else {
                        *adj_pos1 = pos1;
                        *adj_pos2 = pos2;
                        *adj_pos3 = j;
                        return i;
                    }
                }
            }
        }
    }
    return -1;
}

/* get 2 vertex with degree 3 and that shares two common neighbours */
int get_vertex_with_degree_3_two_shared(MatrizAdjacencia M, uint v[], int *other_pos, int *adj_count) {
    int i, j, shared_count;
    
    for (i = 0; i < N; i++) {
        if (v[i] == 0) {
            for (j = 0; j < N; j++) {
                if (i != j && v[j] == 0) {
                    shared_count = get_shared_adjacent_count(M, i, j);
                    if (shared_count >= 2) {
                        *other_pos = j;
                        *adj_count = (shared_count == 2) ? 4 : 3;
                        return i;
                    }
                }
            }
        }
    }
    return -1;
}

/* get vertex which neighbours form a clique */
int get_clique_vertex(MatrizAdjacencia M, uint v[], int *out_count) {
    int i, j, k, is_clique, adj_count;
    
    for (i = 0; i < N; i++) {
        if (v[i] == 0) {
            adj_count = 0;
            is_clique = 1;
            for (j = 0; j < N; j++) {
                if (M[i][j] == 1) {
                    for (k = j + 1; k < N; k++) {
                        if (M[i][k] == 1 && M[j][k] != 1) {
                            is_clique = 0;
                            break;
                        }
                    }
                    if (is_clique == 0) {
                        break;
                    }
                    adj_count++;
                }
            }
            if (is_clique == 0) {
                continue;
            }
            else {
                *out_count = adj_count;
                return i;
            }
        }
    }
    return -1;
}

/* count the number of shared adjacent vertices */
int get_shared_adjacent_count(MatrizAdjacencia M, int pos1, int pos2) {
    int i, shared_count = 0;
    
    for (i = 0; i < N; i++) {
        if (M[pos1][i] == 1 && M[pos2][i] == 1) {
            shared_count++;
        }
    }
    return shared_count;
}

/* count the number of adjacent vertices by one vertice's neighbours */
int get_adjacent_adjacent_count(MatrizAdjacencia M, int pos) {
    int i, pos1 = -1, pos2 = -1, adj_count = 0;
    
    for (i = 0; i < N; i++) {
        if (M[pos][i] == 1) {
            if (pos1 == -1) {
                pos1 = i;
            }
            else {
                pos2 = i;
            }
        }
    }
    
    for (i = 0; i < N; i++) {
        if (M[pos1][i] == 1 || M[pos2][i] == 1) {
            adj_count++;
        }
    }
    return adj_count;
}

/* remove V from the adjacency matrix and degree from its adjacents */
void remove_vertex(MatrizAdjacencia M, int pos) {
    uint i;
    for (i = 0; i < N; i++) {
        /* vertex adjacent to v */
        if (M[i][pos] == 1) {
            M[i][pos] = 0;
            M[pos][i] = 0;
        }
    }    
}

/* removes V and adjacent vertices from the adjacency matrix */
void add_vertex_to_solution(MatrizAdjacencia M, uint v[], int pos, int adj_count) {
    int i;
    
    /* removes adjacent vertices */
    if (adj_count != 0) {
        for (i = 0; i < N; i++) {
            if (M[i][pos] == 1) {
                v[i] = 2;
                remove_vertex(M, i);
                if (--adj_count == 0) {
                    break;
                }
            }
        }
    }
    /* adds V to solution */
    v[pos] = 1;
}

/* removes V and adjacent vertices from the adjacency matrix */
void add_adjacent_to_solution(MatrizAdjacencia M, uint v[], int pos) {
    int i, j;
    /* removes adjacent's adjacent vertices */
    for (i = 0; i < N; i++) {
        /* adjacent */
        if (M[i][pos] == 1) {
            for (j = 0; j < N; j++) {
                /* adjacent's adjacent */
                if (j != pos && M[i][j] == 1) {
                    v[j] = 2;
                    remove_vertex(M, j);
                }
            }
            /* adds adjacent to solution */
            v[i] = 1;
        }
    }
    remove_vertex(M, pos);
    v[pos] = 2;
}

void zero_array(uint *v, int n) {
    int i;
    for (i = 0; i < n; i++) {
        v[i] = 0;
    }
}

void copy_array(uint *vc, uint *vo, int n) {
    int i;
    for (i = 0; i < n; i++)
        vc[i] = vo[i];
}

void zero_matrix(MatrizAdjacencia M, int n) {
    int i, j;
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            M[i][j] = 0;
        }
    }
}

void copy_matrix(MatrizAdjacencia Mc, MatrizAdjacencia Mo, int n) {
    int i, j;
    for (i = 0; i < n; i++)
        for (j = 0; j < n; j++)
            Mc[i][j] = Mo[i][j];
}

void add_edge(MatrizAdjacencia M, int n, int m) {
    M[n][m] = M[m][n] = 1;
}

void remove_edge(MatrizAdjacencia M, int n, int m) {
    M[n][m] = M[m][n] = 0;
}

void print_seq_file(FILE *out, uint seq[], int n) {
    int i;
    for (i = 0; i < n; i++) {
        if (seq[i] == 1)
            fprintf(out, " %d", i);
    }
    fprintf(out, "\n");
}

void alloc_array(uint **p, int n) {
    *p = (uint *) malloc(n * sizeof(uint));
}

void alloc_matrix(uint ***p, int m, int n) {
    int i;
    *p = (uint **) malloc(m * sizeof(uint*)); 
    for (i = 0; i < m; i++)
        (*p)[i] = (uint *) malloc(n * sizeof(uint));
}

void free_matrix(uint **p, int m) {
    int i;
    for (i = 0; i < m; i++)
        free(p[i]);
    free(p);
}

void freeA1_A2(uint *v0, uint *v1, uint **M0, MatrizAdjacencia M1) {
    free(v0);
    free(v1);
    free_matrix(M0, N);
    free_matrix(M1, N);
}

void freeA3(uint *v1, uint *v2, uint *v3, uint *v4, Madj M1, Madj M2, Madj M3, Madj M4) {
    free(v1);
    free(v2);
    free(v3);
    free(v4);
    free_matrix(M1, N);
    free_matrix(M2, N);
    free_matrix(M3, N);
    free_matrix(M4, N);
}

int main(int argc, char *argv[]) {
    int i, j, v1, v2;
    int NG, n, m;
    MatrizAdjacencia M, M_copy;
    
    uint tam_ci[3], tempo_ok[3], **seq_vert_ci;
    
    /* analysis variables */
    uint A1_time, A2_time, A3_time, max_time;
    A1_time = A2_time = A3_time = 0;
    clock_t time = clock();
    
    /* I/O variables */
    FILE *data_file;
    FILE *file_out;
    char file_name[50];
    
    /* tests if there's two arguments */
    if (argc != 3) {
        printf("Expecting two arguments.\n");
        return 1;
    }
    
    /* tries to open argument file */
    data_file = fopen(argv[1], "r");
    if (data_file == NULL) {
        return 1;
    }
    
    /* generates output file */
    strcpy(file_name, "ra118363_118557.log");
    if (access(file_name, F_OK) != -1)
        file_out = fopen(file_name, "a");
    else
        file_out = fopen(file_name, "w");
    
    /* gets max time allowed from argument */
    sscanf(argv[2], " %d", &max_time);
    
    /* reads number of graphs, vertices and edges */
    fscanf(data_file, " %d %d %d", &NG, &n, &m);
    N = n;
    
    alloc_matrix(&M, n, n);
    alloc_matrix(&M_copy, n, n);
    alloc_matrix(&seq_vert_ci, 3, n);
    
    for (i = 0; i < NG; i++) {
        zero_array(n_call, 3);
        zero_array(tam_ci, 3);
        zero_array(seq_vert_ci[0], n);
        zero_array(seq_vert_ci[1], n);
        zero_array(seq_vert_ci[2], n);
        
        /* reads all edges from graph */
        zero_matrix(M, n);
        for (j = 0; j < m; j++) {
            fscanf(data_file, " %d %d", &v1, &v2);
            add_edge(M, v1, v2);
        }
        
        /* finds independent set using A1 algorithm */
        copy_matrix(M_copy, M, n);
        n_call[0]++;
        time = clock();
        tempo_ok[0] = A1(n, M_copy, &(tam_ci[0]), seq_vert_ci[0], max_time, 1);
        A1_time += clock() - time;
        
        /* finds independent set using A2 algorithm */
        copy_matrix(M_copy, M, n);
        n_call[1]++;
        time = clock();
        tempo_ok[1] = A2(n, M_copy, &(tam_ci[1]), seq_vert_ci[1], max_time);
        A2_time += clock() - time;
        
        /* finds independent set using A3 algorithm */
        copy_matrix(M_copy, M, n);
        n_call[2]++;
        time = clock();
        tempo_ok[2] = A3(n, M_copy, &(tam_ci[2]), seq_vert_ci[2], max_time, 0);
        A3_time += clock() - time;
        
        /* log file output */
        fprintf(file_out, "ra118363_118557 A1 %s %d %d %d %u %u %u", argv[1], i + 1,
            n, m, tempo_ok[0], n_call[0], tam_ci[0]);
        print_seq_file(file_out, seq_vert_ci[0], n);
        fprintf(file_out, "ra118363_118557 A2 %s %d %d %d %u %u %u", argv[1], i + 1,
            n, m, tempo_ok[1], n_call[1], tam_ci[1]);
        print_seq_file(file_out, seq_vert_ci[1], n);
        fprintf(file_out, "ra118363_118557 A3 %s %d %d %d %u %u %u", argv[1], i + 1,
            n, m, tempo_ok[2], n_call[2], tam_ci[2]);
        print_seq_file(file_out, seq_vert_ci[2], n);
    }
    
    free_matrix(M, N);
    free_matrix(M_copy, N);
    free_matrix(seq_vert_ci, 3);
    
    fclose(data_file);
    fclose(file_out);
    
    return 0;
}
