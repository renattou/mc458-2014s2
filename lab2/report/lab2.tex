\documentclass[a4paper,titlepage]{article}

\usepackage[margin=1in]{geometry}
\usepackage[brazilian]{babel}
\usepackage[utf8]{inputenc}
\usepackage{caption}
\usepackage{fixltx2e}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{amsmath}
\usepackage{array}
\usepackage{hyperref}

\MakeRobust{\Call}

\title{MC458 - Relatório Laboratório 02}

\author{Pedro Gabriel Calixto \\ RA: 118363 \and Renato Landim Vargas \\ RA: 118557}

\date{27 de outubro de 2014}

\begin{document}

\maketitle

\tableofcontents

\section{Introdução}

O problema proposto para o laboratório 2 foi a resolução do \textbf{Problema do Conjunto Independente Máximo (CIM)}. Dado um grafo $G$ não orientado, dizemos que um subconjunto $S$ dos vértices de $G$ é um conjunto independente de $G$ se quaisquer dois vértices distintos de $S$ não está ligado por uma aresta de $G$. O problema consiste em encontrar este subconjunto.

Foram implementados dois algoritmos mostrados no enunciado (A1 e A2) e outro algoritmo da literatura com menor complexidade (A3). Foi desenvolvido um programa em C executando os três algoritmos que proporciona resultados comparativos entre eles.

\section{Modelagem}

\subsection{Notação}

\begin{itemize}
\item $G$ é o grafo de entrada, ou seja, conjunto de vértices e arestas.
\item $n$ é o número de vértices de $G$
\item $\textsc{N}(u, A)$ é o conjunto de vizinhos de $u$ em um subconjunto $A$.
\item $\textsc{T}(n)$ é a complexidade de tempo da chamada em questão.
\item $\textsc{max}(A, B, ...)$ é uma função que retorna o conjunto com maior número de vértices.
\item $\textsc{is\_clique}(A)$ é uma função que retorna se um conjunto forma um clique, i.e., cada dois vértices do conjunto são conectados por uma aresta.
\end{itemize}

\newpage
\subsection{Algoritmo A1}

O método A1 (Algoritmo \ref{a1}) é o algoritmo recursivo mais simples para encontrar o CIM. Para um vértice $v$ em $G$, são considerados os dois casos possíveis: $v$ está na solução ou não está. Após a execução das duas chamadas recursivas, é escolhido o resultado com maior subconjunto S. Caso o vértice seja de grau $0$, ele já é adicionado na solução.

\begin{algorithm}[H]
\caption{A1}\label{a1}
\begin{algorithmic}[1]
\Procedure{A1}{$G$} \Comment{Graph $G$ with $n$ vertices}
	\If {$|G| = 0$} \Comment{Independent set found}
		\State \Return $G$
	\EndIf
	\State $u \in G, A = \Call{N}{u, G}$
	\If {$|A| = 0$} \Comment{\textbf{Case 1}}
		\State \Return $\{u\} \cup \Call{A1}{G-\{u\}}$ \Comment{$\Call{T}{n-1}$}
	\Else \Comment{\textbf{Case 2}}
		\State $G_0 \gets \Call{A1}{G-\{u\}}$ \Comment{$\Call{T}{n-1}$}
		\State $G_1 \gets \{u\} \cup \Call{A1}{G-\{u\}-A}$ \Comment{$\Call{T}{n-2}$}
		\State \Return $\Call{max}{G_0, G_1}$
	\EndIf
\EndProcedure
\end{algorithmic}
\end{algorithm}

Temos então que o tempo de complexidade da função é:
\begin{equation*}
\begin{split}
T_1(n) & = max(T_1(n-1); T_1(n-1) + T_1(n-2)) \\
& = T_1(n-1) + T_1(n-2)
\end{split}
\end{equation*}

Resolvendo essa recorrência, temos que A1 possui complexidade $O^*(1.618\ldots^n)$.

\newpage
\subsection{Algoritmo A2}

O método A2 (Algoritmo \ref{a2}) proporciona algumas otimizações em relação ao A1 que diminuem drasticamente a sua complexidade. Para cada chamada recursiva, são testados os seguintes casos (até que um seja válido):

\begin{itemize}
\item \textbf{Caso 1}: sempre que possível, são selecionados vértices de grau $0$ ou $1$. Esses vértices podem sempre serem adicionados à solução.
\item \textbf{Caso 2}: se houver um vértice de grau maior ou igual a $3$, são considerados os casos em que ele está ou não na solução.
\item \textbf{Caso 3}: nesse caso, todos os vértices são de grau $2$. É possível resolver esse caso em tempo polinomial. Qualquer vértice pode ser escolhido e adicionado a solução. 
\end{itemize}

\begin{algorithm}[H]
\caption{A2}\label{a2}
\begin{algorithmic}[1]
\Procedure{A2}{$G$} \Comment{Graph $G$ with $n$ vertices}
	\If {$|G| = 0$} \Comment{Independent set found}
		\State \Return $G$
	\ElsIf {$\exists u \in G (A = \Call{N}{u, G}, |A| = 0)$} \Comment{\textbf{Case 1}}
		\State \Return $\{u\} \cup \Call{A2}{G-\{u\}}$ \Comment{$\Call{T}{n-1}$}
	\ElsIf {$\exists u \in G (A = \Call{N}{u, G}, |A| = 1)$} \Comment{\textbf{Case 2}}
		\State \Return $\{u\} \cup \Call{A2}{G-\{u\}-A}$ \Comment{$\Call{T}{n-2}$}
	\ElsIf {$\exists u \in G (A = \Call{N}{u, G}, |A| \geq 3)$} \Comment{\textbf{Case 3}}
		\State $G_0 \gets \Call{A2}{G-\{u\}}$ \Comment{$\Call{T}{n-1}$}
		\State $G_1 \gets \{u\} \cup \Call{A2}{G-\{u\}-A}$ \Comment{$\Call{T}{n-4}$}
		\State \Return $\Call{max}{G_0, G_1}$
	\Else \Comment{\textbf{Case 4}}
		\State $u \in G, A = \Call{N}{u, G}$ \Comment{$|A| = 2$}
		\State \Return $\{u\} \cup \Call{A2}{G-\{u\}-A}$ \Comment{$\Call{T}{n-3}$}
	\EndIf
\EndProcedure
\end{algorithmic}
\end{algorithm}

Temos então que o tempo de complexidade da função é:
\begin{equation*}
\begin{split}
T_2(n) & = max(T_2(n-1); T_2(n-2); T_2(n-1) + T_2(n-4); T_2(n-3)) \\
& = T_2(n-1) + T_2(n-4)
\end{split}
\end{equation*}

Resolvendo essa recorrência, temos que A2 possui complexidade $O^*(1.380\ldots^n)$.

\newpage
\subsection{Algoritmo A3}

O método A3 (Algoritmo \ref{a3}) é mais sofisticado e considera mais casos que o A2 para diminuir a sua complexidade. Ele foi baseado nos slides de aula de P. Berman \cite{berman} Para cada chamada recursiva, são testados os seguintes casos (até que um seja válido):

\begin{itemize}
\item \textbf{Caso 1}: se os vizinhos de um vértice formarem um clique, esse vértice é adicionado à solução. Se qualquer outro dos vizinhos estivesse na solução, ele poderia ser trocado pelo vértice em questão.
\item \textbf{Caso 2}: se houver um vértice de grau $2$ que o conjunto dos vizinhos dos seus vizinhos tenha cardinalidade $2$, os dois vizinhos são adicionados à solução. Como o Caso 1 não foi satisfeito, é garantido que os dois vizinhos não estão conectados entre si.
\item \textbf{Caso 3}: se houver um vértice de grau $2$, são considerados os casos em que ele está na solução ou que seus vizinhos estão. Como o Caso 2 não foi satisfeito, o conjunto de vizinhos dos vizinhos tem cardinalidade pelo menos $3$.
\item \textbf{Caso 4}: se houver um vértice com grau maior ou igual a $4$, são considerados os casos em que ele está na solução ou não.
\item Os próximos casos possuem apenas vértices de grau $3$.
\item \textbf{Caso 5}: se houver um vértice em que (apenas) dois dos seus vizinhos estão conectados entre si, são considerados os casos em que o vértice está na solução ou que o vizinho não conectado aos outros está.
\item \textbf{Caso 6}: se dois vértices compartilham pelo menos dois vizinhos em comum, são considerados os casos em que os dois vértices estão na solução ou que nenhum dos dois está. Como o Caso 5 não foi satisfeito, os dois vértices não estão conectados.
\item \textbf{Caso 7}: nesse caso, todo vértice está conectado a três outros que não compartilham nenhum outro vizinho. São considerados o caso em que o vértice está na solução e os casos que incluem a combinação dois a dois dos seus vizinhos.
\end{itemize}

\begin{algorithm}[H]
\caption{A3}\label{a3}
\begin{algorithmic}[1]
\Procedure{A3}{$G$} \Comment{Graph $G$ with $n$ vertices}
	\If {$|G| = 0$} \Comment{Independent set found}
		\State \Return $G$
	\ElsIf {$\exists u \in G (A = \Call{N}{u, G}, \Call{is\_clique}{A})$} \Comment{\textbf{Case 1}}
		\State \Return $\{u\} \cup \Call{A3}{G-\{u\}}$ \Comment{$\Call{T}{n-1}$}
	\ElsIf {$\exists u \in G ((A = \Call{N}{u, G}, |A| = 2) \land (B = \Call{N}{A, G}, |B| = 2))$} \Comment{\textbf{Case 2}}
		\State \Return $A \cup \Call{A3}{G-A-B}$ \Comment{$T(n-4)$}
	\ElsIf {$\exists u \in G (A = \Call{N}{u, G}, |A| = 2)$} \Comment{\textbf{Case 3}}
		\State $B = \Call{N}{u, A}$
		\State $G_0 \gets \{u\} \cup \Call{A3}{G-\{u\}-A}$ \Comment{$\Call{T}{n-3}$}
		\State $G_1 \gets A \cup \Call{A3}{G-A-B}$ \Comment{$\Call{T}{n-5}$}
		\State \Return $\Call{max}{G_0, G_1}$
	\ElsIf {$\exists u \in G (A = \Call{N}{u, G}, |A| \geq 4)$} \Comment{\textbf{Case 4}}
		\State $G_0 \gets \Call{A3}{G-\{u\}}$ \Comment{$\Call{T}{n-1}$}
		\State $G_1 \gets \{u\} \cup \Call{A3}{G-\{u\}-A}$ \Comment{$\Call{T}{n-5}$}
		\State \Return $\Call{max}{G_0, G_1}$
	\ElsIf {$\exists u \in G (A = \Call{N}{u, G}, \exists v \in A (B = \Call{N}{v, G}, A \cap B = \emptyset))$} \Comment{\textbf{Case 5}, $|A| = 3$}
		\State $G_0 \gets \{u\} \cup \Call{A3}{G-\{u\}-A}$ \Comment{$\Call{T}{n-4}$}
		\State $G_1 \gets \{v\} \cup \Call{A3}{G-\{v\}-B}$ \Comment{$\Call{T}{n-4}$}
		\State \Return $\Call{max}{G_0, G_1}$
	\ElsIf {$\exists u, v \in G (A = \Call{N}{u, G}, B = \Call{N}{v, G}, |A \cap B| = 2)$} \Comment{\textbf{Case 6}, $|A| = 3$}
		\State $G_0 \gets \Call{A3}{G-\{u, v\}}$ \Comment{$\Call{T}{n-2}$}
		\State $G_1 \gets \{u, v\} \cup \Call{A3}{G-\{u, v\}-A-B}$ \Comment{$\Call{T}{n-5}$}
		\State \Return $\Call{max}{G_0, G_1}$
	\Else \Comment{\textbf{Case 7}}
		\State $u \in G, \{v, w, x\} = \Call{N}{u, G}$ \Comment{$|A| = 3$}
		\State $G_0 \gets \{u\} \cup \Call{A3}{G-\{u\}-\Call{N}{u, G}}$ \Comment{$\Call{T}{n-4}$}
		\State $G_1 \gets \{v, w\} \cup \Call{A3}{G-\{v, w\}-\Call{N}{v, G}-\Call{N}{w, G}}$ \Comment{$\Call{T}{n-7}$}
		\State $G_2 \gets \{v, x\} \cup \Call{A3}{G-\{v, x\}-\Call{N}{v, G}-\Call{N}{x, G}}$ \Comment{$\Call{T}{n-7}$}
		\State $G_3 \gets \{w, x\} \cup \Call{A3}{G-\{w, x\}-\Call{N}{w, G}-\Call{N}{x, G}}$ \Comment{$\Call{T}{n-7}$}
		\State \Return $\Call{max}{G_0, G_1, G_2, G_3}$
	\EndIf
\EndProcedure
\end{algorithmic}
\end{algorithm}

Temos então que o tempo de complexidade da função é:
\begin{equation*}
\begin{split}
T_3(n) & = max(T_3(n-1); T_3(n-4); T_3(n-3) + T_3(n-5); T_3(n-1) + T_3(n-5);\\
&\quad \ 2 * T_3(n-4); T_3(n-2) + T_3(n-5); T_3(n-4) + 3 * T_3(n-7)) \\
& = T_3(n-1) + T_3(n-5)
\end{split}
\end{equation*}

Resolvendo essa recorrência, temos que A3 possui complexidade $O^*(1.324\ldots^n)$.


\newpage
\subsection{Entrada e Saída}

A entrada esperada está no formato:

\begin{center}
$\begin{matrix}
NG & n & m & & \\
e_{G_1,1} & e_{G_1,2} & e_{G_1,3} & \cdots & e_{G_1,m} \\
e_{G_2,1} & e_{G_2,2} & e_{G_2,3} & \cdots & e_{G_2,m} \\
\vdots & \vdots & \vdots & \ddots & \vdots \\
e_{G_{NG},1} & e_{G_{NG},2} & e_{G_{NG},3} & \cdots & e_{G_{NG},m} \\
\end{matrix}$
\end{center}

Onde $NG$ é o número de grafos no arquivo; $n$ é o número de vértices em cada grafo; $m$ é o número de arestas; e $e_{G_i,j}$ é um par $e_u e_v$, indicando uma aresta entre os vértices $u$ e $v$.

Enquanto a saída esperada está no formato:

\begin{table}[h]
\centering
  \begin{tabular}{>{\em}l >{\em}l >{\em}l >{\em}c >{\em}l >{\em}l >{\em}l >{\em}l >{\em}l >{\em}l}
    RA & A1 & arquivo & 1 & n & m & ótimo & número\_chamadas & tamanho\_ci & vértices\_ci \\
    RA & A2 & arquivo & 1 & n & m & ótimo & número\_chamadas & tamanho\_ci & vértices\_ci \\
    RA & A3 & arquivo & 1 & n & m & ótimo & número\_chamadas & tamanho\_ci & vértices\_ci \\
    RA & A1 & arquivo & 2 & n & m & ótimo & número\_chamadas & tamanho\_ci & vértices\_ci \\
    RA & A2 & arquivo & 2 & n & m & ótimo & número\_chamadas & tamanho\_ci & vértices\_ci \\
    RA & A3 & arquivo & 2 & n & m & ótimo & número\_chamadas & tamanho\_ci & vértices\_ci \\
    \vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots  \\
    RA & A1 & arquivo & NG & n & m & ótimo & número\_chamadas & tamanho\_ci & vértices\_ci \\
    RA & A2 & arquivo & NG & n & m & ótimo & número\_chamadas & tamanho\_ci & vértices\_ci \\
    RA & A3 & arquivo & NG & n & m & ótimo & número\_chamadas & tamanho\_ci & vértices\_ci \\
  \end{tabular}
\end{table}

Onde $RA$ é o RA dos membros da equipe na forma $raXXXXXX\_YYYYYY$; $A1$, $A2$ e $A3$ representam os dados para os respectivos métodos; $arquivo$ é o nome do arquivo de entrada; $1, 2, \ldots, NG$ é o número do grafo no arquivo; $n$ é o número de vértices do grafo; $m$ é o número de arestas no grafo; \emph{ótimo} é igual a $1$ se terminou dentro do tempo e a solução é ótima ou é $0$ caso contrário; \emph{número\_chamadas} é o número de chamadas recursivas do método; $tamanho\_ci$ é a cardinalidade do conjunto independente encontrado; \emph{vértices\_ci} é a sequência de vértices do conjunto independente encontrado.

\section{Estrutura}

O programa recebe como entrada pela linha de comando um arquivo de texto contendo uma lista de grafos com mesmo número de vértices e arestas. O segundo argumento do programa é o tempo máximo de execução. Caso o método não tenha finalizado dentro desse tempo, ele é parado sem garantir que o grafo encontrado é o CIM.

Para cada grafo lido, é montada uma matriz de adjacência. Essa matriz alimenta os três métodos implementados. Alguns dados comparativos de cada método são gravados em um log para posterior análise. Caso o arquivo de log não exista, ele é criado e os resultados são colocados nele.

\section{Resultados}

O programa foi executado numa máquina com CPU de 4 \textit{cores} de Intel\textsuperscript{®} Core\textsuperscript{TM} i5-3230M CPU @ 2.60GHz, sistema operacional de 64-bit, 7.7 GB de memória RAM, compilado usando o compilador $gcc$ na versão $4.8.2$, com as flags ``-Wall -ansi -lm'', sem nenhuma otimização.

\section{Conclusão}

Através desse laboratório, foi possível mostrar a relação entre complexidade e tempo de execução. Em todos os testes, o A2 teve melhor desempenho que o A1. Da mesma forma, o A3 foi melhor que o A2. Mesmo que os métodos tenham um código mais extenso, fazendo mais buscas pelos vértices para cada caso, o desempenho em tempo e número de chamadas compensam isso.

\begin{thebibliography}{9}

\bibitem{berman}
Berman, Piotr. ``Recursive algorithm for Maximum Independent Set Problem''. \emph{CSE465, Spring 2009}. Acessado em outubro de 2014. \url{http://www.cse.psu.edu/~berman/lecture_01_28.pdf}

\bibitem{wikimsi}
Wikipedia, the free encyclopedia. ``Independent set (graph theory)''. Acessado em outubro de 2014 \url{http://en.wikipedia.org/wiki/Independent_set_(graph_theory)}

\end{thebibliography}

\end{document}
